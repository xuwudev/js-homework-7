// 1. forEach перебирає масив
// 2. або переписати масив, або задати йому довжину 0, або splice, або pop
// 3. typeof

function filterBy(arr, dataType) {
  return arr.filter(function (item) {
    return typeof item !== dataType
  })
}

let inputArray = ["hello", "world", 23, "23", null]
let resultArray = filterBy(inputArray, "string")

console.log(resultArray)
